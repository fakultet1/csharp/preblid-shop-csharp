﻿namespace AplikacijaPrebild
{
    partial class Proizvodi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnNeprehrambeni = new System.Windows.Forms.Button();
            this.btnPrehrambeni = new System.Windows.Forms.Button();
            this.btnSvi = new System.Windows.Forms.Button();
            this.btnDodajNovi = new System.Windows.Forms.Button();
            this.btnUredi = new System.Windows.Forms.Button();
            this.btnIzbrisi = new System.Windows.Forms.Button();
            this.btnSpremi = new System.Windows.Forms.Button();
            this.btnNazad = new System.Windows.Forms.Button();
            this.listViewPopisProizvoda = new System.Windows.Forms.ListView();
            this.SuspendLayout();
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(0, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(241, 561);
            this.splitter1.TabIndex = 0;
            this.splitter1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(400, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Popis proizvoda:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(11, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(211, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Izaberite vrstu proizvoda:";
            // 
            // btnNeprehrambeni
            // 
            this.btnNeprehrambeni.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnNeprehrambeni.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNeprehrambeni.Location = new System.Drawing.Point(22, 50);
            this.btnNeprehrambeni.Name = "btnNeprehrambeni";
            this.btnNeprehrambeni.Size = new System.Drawing.Size(200, 30);
            this.btnNeprehrambeni.TabIndex = 4;
            this.btnNeprehrambeni.Text = "Neprehrambeni proizvodi";
            this.btnNeprehrambeni.UseVisualStyleBackColor = false;
            this.btnNeprehrambeni.Click += new System.EventHandler(this.btnNeprehrambeni_Click);
            // 
            // btnPrehrambeni
            // 
            this.btnPrehrambeni.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnPrehrambeni.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrehrambeni.Location = new System.Drawing.Point(22, 99);
            this.btnPrehrambeni.Name = "btnPrehrambeni";
            this.btnPrehrambeni.Size = new System.Drawing.Size(200, 30);
            this.btnPrehrambeni.TabIndex = 5;
            this.btnPrehrambeni.Text = "Prehrambeni proizvodi";
            this.btnPrehrambeni.UseVisualStyleBackColor = false;
            this.btnPrehrambeni.Click += new System.EventHandler(this.btnPrehrambeni_Click);
            // 
            // btnSvi
            // 
            this.btnSvi.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnSvi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSvi.Location = new System.Drawing.Point(22, 148);
            this.btnSvi.Name = "btnSvi";
            this.btnSvi.Size = new System.Drawing.Size(200, 30);
            this.btnSvi.TabIndex = 6;
            this.btnSvi.Text = "Svi proizvodi";
            this.btnSvi.UseVisualStyleBackColor = false;
            this.btnSvi.Click += new System.EventHandler(this.btnSvi_Click);
            // 
            // btnDodajNovi
            // 
            this.btnDodajNovi.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnDodajNovi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDodajNovi.Location = new System.Drawing.Point(22, 196);
            this.btnDodajNovi.Name = "btnDodajNovi";
            this.btnDodajNovi.Size = new System.Drawing.Size(200, 30);
            this.btnDodajNovi.TabIndex = 7;
            this.btnDodajNovi.Text = "Dodaj novi proizvod";
            this.btnDodajNovi.UseVisualStyleBackColor = false;
            this.btnDodajNovi.Click += new System.EventHandler(this.btnDodajNovi_Click);
            // 
            // btnUredi
            // 
            this.btnUredi.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnUredi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUredi.Location = new System.Drawing.Point(22, 244);
            this.btnUredi.Name = "btnUredi";
            this.btnUredi.Size = new System.Drawing.Size(200, 30);
            this.btnUredi.TabIndex = 8;
            this.btnUredi.Text = "Uredi proizvod";
            this.btnUredi.UseVisualStyleBackColor = false;
            this.btnUredi.Click += new System.EventHandler(this.btnUredi_Click);
            // 
            // btnIzbrisi
            // 
            this.btnIzbrisi.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnIzbrisi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIzbrisi.ForeColor = System.Drawing.Color.Red;
            this.btnIzbrisi.Location = new System.Drawing.Point(22, 292);
            this.btnIzbrisi.Name = "btnIzbrisi";
            this.btnIzbrisi.Size = new System.Drawing.Size(200, 30);
            this.btnIzbrisi.TabIndex = 9;
            this.btnIzbrisi.Text = "Izbriši proizvod";
            this.btnIzbrisi.UseVisualStyleBackColor = false;
            this.btnIzbrisi.Click += new System.EventHandler(this.btnIzbrisi_Click);
            // 
            // btnSpremi
            // 
            this.btnSpremi.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnSpremi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSpremi.ForeColor = System.Drawing.Color.Green;
            this.btnSpremi.Location = new System.Drawing.Point(22, 340);
            this.btnSpremi.Name = "btnSpremi";
            this.btnSpremi.Size = new System.Drawing.Size(200, 30);
            this.btnSpremi.TabIndex = 10;
            this.btnSpremi.Text = "Spremi promjene";
            this.btnSpremi.UseVisualStyleBackColor = false;
            this.btnSpremi.Click += new System.EventHandler(this.btnSpremiPromjene_Click);
            // 
            // btnNazad
            // 
            this.btnNazad.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnNazad.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNazad.Location = new System.Drawing.Point(22, 387);
            this.btnNazad.Name = "btnNazad";
            this.btnNazad.Size = new System.Drawing.Size(200, 30);
            this.btnNazad.TabIndex = 11;
            this.btnNazad.Text = "Nazad";
            this.btnNazad.UseVisualStyleBackColor = false;
            this.btnNazad.Click += new System.EventHandler(this.btnNazad_Click);
            // 
            // listViewPopisProizvoda
            // 
            this.listViewPopisProizvoda.HideSelection = false;
            this.listViewPopisProizvoda.Location = new System.Drawing.Point(247, 50);
            this.listViewPopisProizvoda.Name = "listViewPopisProizvoda";
            this.listViewPopisProizvoda.Size = new System.Drawing.Size(525, 499);
            this.listViewPopisProizvoda.TabIndex = 12;
            this.listViewPopisProizvoda.UseCompatibleStateImageBehavior = false;
            this.listViewPopisProizvoda.View = System.Windows.Forms.View.List;
            // 
            // Proizvodi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.listViewPopisProizvoda);
            this.Controls.Add(this.btnNazad);
            this.Controls.Add(this.btnSpremi);
            this.Controls.Add(this.btnIzbrisi);
            this.Controls.Add(this.btnUredi);
            this.Controls.Add(this.btnDodajNovi);
            this.Controls.Add(this.btnSvi);
            this.Controls.Add(this.btnPrehrambeni);
            this.Controls.Add(this.btnNeprehrambeni);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.splitter1);
            this.Name = "Proizvodi";
            this.Text = "Proizvodi";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnNeprehrambeni;
        private System.Windows.Forms.Button btnPrehrambeni;
        private System.Windows.Forms.Button btnSvi;
        private System.Windows.Forms.Button btnDodajNovi;
        private System.Windows.Forms.Button btnUredi;
        private System.Windows.Forms.Button btnIzbrisi;
        private System.Windows.Forms.Button btnSpremi;
        private System.Windows.Forms.Button btnNazad;
        private System.Windows.Forms.ListView listViewPopisProizvoda;
    }
}