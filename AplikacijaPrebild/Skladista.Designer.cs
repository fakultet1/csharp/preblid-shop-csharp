﻿namespace AplikacijaPrebild
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPopis = new System.Windows.Forms.Label();
            this.listView1 = new System.Windows.Forms.ListView();
            this.lblInfo = new System.Windows.Forms.Label();
            this.lblNaziv = new System.Windows.Forms.Label();
            this.lblAdresa = new System.Windows.Forms.Label();
            this.lblOIB = new System.Windows.Forms.Label();
            this.lblKontakt = new System.Windows.Forms.Label();
            this.btnPogledajProizvode = new System.Windows.Forms.Button();
            this.lblNazivInfo = new System.Windows.Forms.Label();
            this.lblAdresaInfo = new System.Windows.Forms.Label();
            this.lblOIBInfo = new System.Windows.Forms.Label();
            this.lblKontaktInfo = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblPopis
            // 
            this.lblPopis.AutoSize = true;
            this.lblPopis.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPopis.Location = new System.Drawing.Point(107, 23);
            this.lblPopis.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPopis.Name = "lblPopis";
            this.lblPopis.Size = new System.Drawing.Size(137, 24);
            this.lblPopis.TabIndex = 1;
            this.lblPopis.Text = "Popis skladišta:";
                      // 
            // listView1
            // 
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(11, 49);
            this.listView1.Margin = new System.Windows.Forms.Padding(2);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(344, 490);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.List;
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // lblInfo
            // 
            this.lblInfo.AutoSize = true;
            this.lblInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInfo.Location = new System.Drawing.Point(491, 32);
            this.lblInfo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(198, 24);
            this.lblInfo.TabIndex = 2;
            this.lblInfo.Text = "Informacije o skladištu:";
            // 
            // lblNaziv
            // 
            this.lblNaziv.AutoSize = true;
            this.lblNaziv.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNaziv.Location = new System.Drawing.Point(381, 94);
            this.lblNaziv.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblNaziv.Name = "lblNaziv";
            this.lblNaziv.Size = new System.Drawing.Size(44, 16);
            this.lblNaziv.TabIndex = 3;
            this.lblNaziv.Text = "Naziv:";
                       // 
            // lblAdresa
            // 
            this.lblAdresa.AutoSize = true;
            this.lblAdresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAdresa.Location = new System.Drawing.Point(381, 132);
            this.lblAdresa.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAdresa.Name = "lblAdresa";
            this.lblAdresa.Size = new System.Drawing.Size(54, 16);
            this.lblAdresa.TabIndex = 4;
            this.lblAdresa.Text = "Adresa:";
            // 
            // lblOIB
            // 
            this.lblOIB.AutoSize = true;
            this.lblOIB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOIB.Location = new System.Drawing.Point(381, 174);
            this.lblOIB.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblOIB.Name = "lblOIB";
            this.lblOIB.Size = new System.Drawing.Size(32, 16);
            this.lblOIB.TabIndex = 5;
            this.lblOIB.Text = "OIB:";
            // 
            // lblKontakt
            // 
            this.lblKontakt.AutoSize = true;
            this.lblKontakt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKontakt.Location = new System.Drawing.Point(381, 219);
            this.lblKontakt.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblKontakt.Name = "lblKontakt";
            this.lblKontakt.Size = new System.Drawing.Size(54, 16);
            this.lblKontakt.TabIndex = 6;
            this.lblKontakt.Text = "Kontakt:";
            // 
            // btnPogledajProizvode
            // 
            this.btnPogledajProizvode.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnPogledajProizvode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPogledajProizvode.Location = new System.Drawing.Point(553, 509);
            this.btnPogledajProizvode.Margin = new System.Windows.Forms.Padding(2);
            this.btnPogledajProizvode.Name = "btnPogledajProizvode";
            this.btnPogledajProizvode.Size = new System.Drawing.Size(200, 30);
            this.btnPogledajProizvode.TabIndex = 7;
            this.btnPogledajProizvode.Text = "Pogledaj proizvode";
            this.btnPogledajProizvode.UseVisualStyleBackColor = false;
            this.btnPogledajProizvode.Click += new System.EventHandler(this.btnPogledajProizvode_Click);
            // 
            // lblNazivInfo
            // 
            this.lblNazivInfo.AutoSize = true;
            this.lblNazivInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNazivInfo.Location = new System.Drawing.Point(502, 94);
            this.lblNazivInfo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblNazivInfo.Name = "lblNazivInfo";
            this.lblNazivInfo.Size = new System.Drawing.Size(0, 16);
            this.lblNazivInfo.TabIndex = 8;
            // 
            // lblAdresaInfo
            // 
            this.lblAdresaInfo.AutoSize = true;
            this.lblAdresaInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAdresaInfo.Location = new System.Drawing.Point(502, 132);
            this.lblAdresaInfo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAdresaInfo.Name = "lblAdresaInfo";
            this.lblAdresaInfo.Size = new System.Drawing.Size(0, 16);
            this.lblAdresaInfo.TabIndex = 9;
            // 
            // lblOIBInfo
            // 
            this.lblOIBInfo.AutoSize = true;
            this.lblOIBInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOIBInfo.Location = new System.Drawing.Point(502, 174);
            this.lblOIBInfo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblOIBInfo.Name = "lblOIBInfo";
            this.lblOIBInfo.Size = new System.Drawing.Size(0, 16);
            this.lblOIBInfo.TabIndex = 10;
            // 
            // lblKontaktInfo
            // 
            this.lblKontaktInfo.AutoSize = true;
            this.lblKontaktInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKontaktInfo.Location = new System.Drawing.Point(502, 219);
            this.lblKontaktInfo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblKontaktInfo.Name = "lblKontaktInfo";
            this.lblKontaktInfo.Size = new System.Drawing.Size(0, 16);
            this.lblKontaktInfo.TabIndex = 11;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.lblKontaktInfo);
            this.Controls.Add(this.lblOIBInfo);
            this.Controls.Add(this.lblAdresaInfo);
            this.Controls.Add(this.lblNazivInfo);
            this.Controls.Add(this.btnPogledajProizvode);
            this.Controls.Add(this.lblKontakt);
            this.Controls.Add(this.lblOIB);
            this.Controls.Add(this.lblAdresa);
            this.Controls.Add(this.lblNaziv);
            this.Controls.Add(this.lblInfo);
            this.Controls.Add(this.lblPopis);
            this.Controls.Add(this.listView1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form2";
            this.Text = "Skladista";
                       this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblPopis;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Label lblInfo;
        private System.Windows.Forms.Label lblNaziv;
        private System.Windows.Forms.Label lblAdresa;
        private System.Windows.Forms.Label lblOIB;
        private System.Windows.Forms.Label lblKontakt;
        private System.Windows.Forms.Button btnPogledajProizvode;
        private System.Windows.Forms.Label lblNazivInfo;
        private System.Windows.Forms.Label lblAdresaInfo;
        private System.Windows.Forms.Label lblOIBInfo;
        private System.Windows.Forms.Label lblKontaktInfo;
    }
}