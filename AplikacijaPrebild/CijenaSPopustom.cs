﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AplikacijaPrebild
{
    internal interface CijenaSPopustom
    {
        void IzracunajCijenuSPopustom(double popust, double cijena); //popust npr. 25% upisuje se 25
    }
}
