﻿namespace AplikacijaPrebild
{
    partial class Dobrodosli
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Dobrodosli));
            this.btnIdiUskladista = new System.Windows.Forms.Button();
            this.lblDobrodosli = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnIdiUskladista
            // 
            this.btnIdiUskladista.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnIdiUskladista.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIdiUskladista.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnIdiUskladista.Location = new System.Drawing.Point(560, 508);
            this.btnIdiUskladista.Margin = new System.Windows.Forms.Padding(2);
            this.btnIdiUskladista.Name = "btnIdiUskladista";
            this.btnIdiUskladista.Size = new System.Drawing.Size(200, 30);
            this.btnIdiUskladista.TabIndex = 1;
            this.btnIdiUskladista.Text = "Pronađi skladište";
            this.btnIdiUskladista.UseVisualStyleBackColor = false;
            this.btnIdiUskladista.Click += new System.EventHandler(this.btnIdiUskladista_Click);
            // 
            // lblDobrodosli
            // 
            this.lblDobrodosli.AutoSize = true;
            this.lblDobrodosli.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblDobrodosli.Font = new System.Drawing.Font("Hadassah Friedlaender", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDobrodosli.ForeColor = System.Drawing.Color.Black;
            this.lblDobrodosli.Location = new System.Drawing.Point(229, 147);
            this.lblDobrodosli.Name = "lblDobrodosli";
            this.lblDobrodosli.Size = new System.Drawing.Size(426, 32);
            this.lblDobrodosli.TabIndex = 3;
            this.lblDobrodosli.Text = "Dobrodošli u Prebild skladišta!";
            this.lblDobrodosli.Click += new System.EventHandler(this.lblDobrodosli_Click);
            // 
            // Dobrodosli
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.lblDobrodosli);
            this.Controls.Add(this.btnIdiUskladista);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Dobrodosli";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnIdiUskladista;
        private System.Windows.Forms.Label lblDobrodosli;
    }
}

