﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;

namespace AplikacijaPrebild
{
    internal class Skladiste : Ispisi
    {
        public String naziv { get; set; }
        public String ulica { get; set; }
        public int kucBr { get; set; }
        public String grad { get; set; }
        public String drzava { get; set; }
        public String OIB { get; set; }
        public String kontakt { get; set; }
        public List<String> listaProizvoda;

        public Skladiste() : this("", "", 0, "", "", "", "", new List<string>()) { }

        public Skladiste(string naziv, string ulica, int kucBr, string grad, string drzava, string OIB, string kontakt, List<string> listaProizvoda)
        {
            this.naziv = naziv;
            this.ulica = ulica;
            this.kucBr = kucBr;
            this.grad = grad;
            this.drzava = drzava;
            this.OIB = OIB;
            this.kontakt = kontakt;
            this.listaProizvoda = listaProizvoda;
        }

        public String getAdresa()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(this.ulica);
            sb.Append(" ");
            sb.Append(this.kucBr);
            sb.Append(", ");
            sb.Append(this.grad);
            sb.Append(", ");
            sb.Append(this.drzava);
            return sb.ToString();
        }


        public String toString()
        {
            return this.naziv + ", " + this.ulica + ", " + this.kucBr + ", " + this.grad;
        }

        public String Ispisi()
        {
            return "Naziv:" + this.naziv + "Ulica:" + this.ulica + "Kucni broj:" + this.kucBr + "Grad:" + this.grad + "OIB:" + this.OIB + "Kontakt:" + this.kontakt;

        }

        public List<Skladiste> citaj()
        {
            try
            {
                //citanje jsona
                string json = File.ReadAllText(@"D:\Fakultet\gitRepositories\preblid-shop-csharp\AplikacijaPrebild\JsonDatoteke\skladista.json");
                List<Skladiste> skladista = JsonConvert.DeserializeObject<List<Skladiste>>(json);
                return skladista;
            } catch {
                throw new Iznimka("Dogodila se pogreška u skladištima-");
            }
        }
    }
}

