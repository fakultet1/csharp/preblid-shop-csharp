﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AplikacijaPrebild
{
    public class Iznimka : Exception
    {
        public Iznimka (string msg)
        {
            Poruka = msg;
        }
        public string Poruka { get; }
    }
}
