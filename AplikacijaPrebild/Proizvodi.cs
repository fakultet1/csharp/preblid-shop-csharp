﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace AplikacijaPrebild
{
    public partial class Proizvodi : Form
    {
        private String nazivSkladista;
        List<Proizvod> proizvodi;
        public Proizvodi()
        {
        }
        public Proizvodi(String nazivSkladista)
        {
            InitializeComponent();
            prikaziProizvode(nazivSkladista);
            this.nazivSkladista = nazivSkladista;
        }

        public Proizvodi(String nazivSkladista, List<Proizvod> proizvodiList)
        {
            InitializeComponent();
            this.proizvodi= proizvodiList;
            prikaziProizvode();
            this.nazivSkladista = nazivSkladista;
        }
        //vracamo se nazad u formu skladiste
        private void btnNazad_Click(object sender, EventArgs e)
        {
            Form2 myForm = new Form2();
            this.Hide();
            myForm.ShowDialog();
            this.Close();
        }


        //otvara se forma za dodavanje proizvoda
        private void btnDodajNovi_Click(object sender, EventArgs e)
        {
            
            ProizvodForm myForm = new ProizvodForm(nazivSkladista,null);
            this.Hide();
            myForm.ShowDialog();
            this.Close();
        }


        //proizvodi se prikazuju odmah nakon odabira skladista, prikazuju se svi proizvodi
        private void prikaziProizvode(String nazivSkladista)
        {
            try
            {
                listViewPopisProizvoda.Clear();
                proizvodi = Proizvod.citajSve(nazivSkladista);
                foreach (var podatak in proizvodi)
                {
                    ListViewItem item = new ListViewItem();
                    item.Text = IspisiProizvod(podatak);
                    item.Tag = podatak;
                    listViewPopisProizvoda.Items.Add(item);
                }
            } catch(Exception e)
            {
                throw new Iznimka("Dogodila se pogreška -"+ e); 
            }
        }
        private void prikaziProizvode()
        {
            try
            {
                listViewPopisProizvoda.Clear();
                foreach (var podatak in proizvodi)
                {
                    ListViewItem item = new ListViewItem();
                    item.Text = IspisiProizvod(podatak);
                    item.Tag = podatak;
                    listViewPopisProizvoda.Items.Add(item);
                }
            }
            catch (Exception e)
            {
                throw new Iznimka("Dogodila se pogreška -" + e);
            }
        }



        //ispisuje sve proizvode pritiskom na gum Svi
        private void btnSvi_Click(object sender, EventArgs e)
        {
            listViewPopisProizvoda.Clear();
            proizvodi = Proizvod.citajSve(nazivSkladista);

            foreach (var podatak in proizvodi)
            {
                ListViewItem item = new ListViewItem();
                item.Text = IspisiProizvod(podatak); //pozivam IspisiProizvod koji ispisuje sve proizvode
                item.Tag = podatak;
                listViewPopisProizvoda.Items.Add(item);

            }
            
        }


        //ispisuje sve Neprehrambene proizvode
        private void btnNeprehrambeni_Click(object sender, EventArgs e)
        {

            listViewPopisProizvoda.Clear();
            proizvodi = Proizvod.citajNeprehrambene(nazivSkladista);

            foreach (var podatak in proizvodi)
            {
                ListViewItem item = new ListViewItem();
                item.Text =IspisiProizvod(podatak);
                item.Tag = podatak;
                listViewPopisProizvoda.Items.Add(item);

            }

        }

        //gumb Prehrambeni proizvodi ispisuje sve prehrambene proizvode
        private void btnPrehrambeni_Click(object sender, EventArgs e)
        {

            listViewPopisProizvoda.Clear();
            proizvodi = Proizvod.citajPrehrambene(nazivSkladista);

            foreach (var podatak in proizvodi)
            {
                ListViewItem item = new ListViewItem();
                item.Text =IspisiProizvod(podatak);
                item.Tag = podatak;
                listViewPopisProizvoda.Items.Add(item);

            }

        }


        //prikaz proizvoda ovisno o vrsti proizvoda
        private String IspisiProizvod(Proizvod proizvod)
        {
            String ispis = null;
            if (proizvod.GetType() == typeof(NeprehrambeniProizvod))
            {
                ispis = ((NeprehrambeniProizvod)proizvod).Ispisi();
            }
            else if (proizvod.GetType() == typeof(PrehrambeniProizvod))
            {
                ispis = ((PrehrambeniProizvod)proizvod).Ispisi();
            }
            return ispis;
        }


        //otvara se gumb Uredi proizvod kada je odabran proizvod
        private void btnUredi_Click(object sender, EventArgs e)
        {
            if (listViewPopisProizvoda.SelectedIndices.Count <= 0)
            {
                MessageBox.Show("Molimo odaberite proizvod.");
                return;
            }
            int intselectedindex = listViewPopisProizvoda.SelectedIndices[0];
            if (intselectedindex >= 0)
            {
                Proizvod odabraniProizvod = (Proizvod)listViewPopisProizvoda.Items[intselectedindex].Tag;

                ProizvodForm myForm = new ProizvodForm(nazivSkladista,odabraniProizvod);

                this.Hide();
                myForm.ShowDialog();
                this.Close();
            }
            
        }

        //delegacija asinkrono otvara novi thread task.Run(();
        private void btnSpremiPromjene_Click(object sender, EventArgs e)
        {
                Task.Run(() => {
                    Proizvod.spremiProizvode(proizvodi, nazivSkladista);
                });

            //Proizvod.spremiProizvode(proizvodi, nazivSkladista);
            MessageBox.Show("Promjene su spremljene.");
        }

        private void btnIzbrisi_Click(object sender, EventArgs e)
        {
            if (listViewPopisProizvoda.SelectedIndices.Count <= 0)
            {
                MessageBox.Show("Molimo odaberite proizvod.");
                return;
            }
            int intselectedindex = listViewPopisProizvoda.SelectedIndices[0];
            if (intselectedindex >= 0)
            {
                listViewPopisProizvoda.Items.RemoveAt(intselectedindex);
                MessageBox.Show("Proizvod je izbrisan!");
            }
            else
            {
                throw new Iznimka("Nije odabran niti jedan proizvod");
            }

        }
    }
}
