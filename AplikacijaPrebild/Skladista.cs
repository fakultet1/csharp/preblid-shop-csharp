﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AplikacijaPrebild
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
            prikaziSkladista();
        }
        private void btnPogledajProizvode_Click(object sender, EventArgs e)
        {

            if (listView1.SelectedIndices.Count <= 0)
            {
                return;
            }
            int intselectedindex = listView1.SelectedIndices[0];
            if (intselectedindex >= 0)
            {
                Skladiste odabranoSkladiste = (Skladiste)listView1.Items[intselectedindex].Tag;
                Proizvodi myForm = new Proizvodi(odabranoSkladiste.naziv);
                this.Hide();
                myForm.ShowDialog();
                this.Close();
            }
           
        }

        private void prikaziSkladista()
        {
            Skladiste skladiste = new Skladiste();
        
            List<Skladiste> list = skladiste.citaj();
            foreach(var podatak in list)
            {
                ListViewItem item = new ListViewItem();
                item.Text = podatak.toString(); 
                item.Tag = podatak;
                listView1.Items.Add(item);
                
            }
        }

     
        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count <= 0)
            {
                return;
            }
            int intselectedindex = listView1.SelectedIndices[0];
            if (intselectedindex >= 0)
            {
                Skladiste odabranoSkladiste = (Skladiste)listView1.Items[intselectedindex].Tag;

                lblNazivInfo.Text = odabranoSkladiste.naziv;
                lblOIBInfo.Text = odabranoSkladiste.OIB;
                lblAdresaInfo.Text = odabranoSkladiste.getAdresa();
                lblKontaktInfo.Text = odabranoSkladiste.kontakt;
            }
        }
    }
}
