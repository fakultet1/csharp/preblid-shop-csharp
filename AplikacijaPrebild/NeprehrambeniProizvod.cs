﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AplikacijaPrebild
{
    internal class NeprehrambeniProizvod : Proizvod, Ispisi
    {
        public bool Jamstvo { get; set; }
        public bool SmartUredaj { get; set; }

        //konstruktori
        public NeprehrambeniProizvod() : base() { }


        public NeprehrambeniProizvod(bool jamstvo, bool smartUredaj)
        {
            this.Jamstvo = jamstvo;
            this.SmartUredaj = smartUredaj;

        }


        //da zna koji konstruktor treba koristiti
        [JsonConstructor]
        public NeprehrambeniProizvod(double cijena, string naziv, string proizvodac, string PDV, long sifraProizvoda,bool jamstvo, bool smartUredaj):base(cijena,naziv,proizvodac,PDV,sifraProizvoda)
        {
            this.Jamstvo = jamstvo;
            this.SmartUredaj=smartUredaj;
        }

        public String Ispisi()
        {
            return "Naziv: " + this.Naziv + ", Cijena: " + this.Cijena + " kn";

        }
    }
}
