﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AplikacijaPrebild
{
    public partial class ProizvodForm : Form
    {
        private String nazivSkladista;
        private Proizvod proizvod;

        public ProizvodForm(String nazivSkladista, Proizvod proizvod)
        {
            InitializeComponent();
            this.nazivSkladista = nazivSkladista;
            this.proizvod = proizvod;
            postaviProizvod(proizvod);
        }

        //vracamo se na formu Proizvodi po nazivu skladista znamo u koje skladiste idemo
        private void btnOdustani_Click(object sender, EventArgs e)
        {
            Proizvodi myForm = new Proizvodi(nazivSkladista);
            this.Hide();
            myForm.ShowDialog();
            this.Close();
        }

        //postavljanje proizvoda i dohvacanje informacija o proizvodu
        private void postaviProizvod(Proizvod proizvod)
        {
            if (proizvod != null)
            {
                txtBoxNaziv.Text = proizvod.Naziv;
                txtBoxProizvodac.Text = proizvod.Proizvodac;
                txtBoxPDV.Text = proizvod.PDV;
                txtBoxSifraProizvoda.Text = proizvod.SifraProizvoda.ToString();
                txtBoxCijena.Text = proizvod.Cijena.ToString();

                if (proizvod.GetType() == typeof(NeprehrambeniProizvod))
                {
                    cmboxVrstaProizvoda.Text = this.cmboxVrstaProizvoda.GetItemText("Neprehrambeni proizvod");
                    chBoxJamstvo.Checked = ((NeprehrambeniProizvod)proizvod).Jamstvo; //kastam iz proizvoda u neprehrambeni tako da vidi jamstvo
                    chBoxSmart.Checked = ((NeprehrambeniProizvod)proizvod).SmartUredaj;
                    txtBoxAlergeni.Hide();
                    txtBoxRokTrajanja.Hide();
                    textBoxNetoKolicina.Hide();

                }
                if (proizvod.GetType() == typeof(PrehrambeniProizvod))
                {
                    cmboxVrstaProizvoda.Text = this.cmboxVrstaProizvoda.GetItemText("Neprehrambeni proizvod");
                    txtBoxAlergeni.Text = ((PrehrambeniProizvod)proizvod).Alergeni;
                    txtBoxRokTrajanja.Text = ((PrehrambeniProizvod)proizvod).RokTrajanja; //kastam iz proizvoda u prehrambeni
                    textBoxNetoKolicina.Text = ((PrehrambeniProizvod)proizvod).NetoKolicina;
                    chBoxSmart.Hide();
                    chBoxJamstvo.Hide();

                }

            }
        }

        private void btnSpremi_Click(object sender, EventArgs e)
        {

            if ("Prehrambeni proizvod".Equals(cmboxVrstaProizvoda.Items[cmboxVrstaProizvoda.SelectedIndex].ToString()))
            {
                PrehrambeniProizvod prehrambeniProizvod = new PrehrambeniProizvod();
                prehrambeniProizvod.Naziv =txtBoxNaziv.Text;
                prehrambeniProizvod.Proizvodac = txtBoxProizvodac.Text;
                prehrambeniProizvod.Cijena= Convert.ToDouble(txtBoxCijena.Text);
                prehrambeniProizvod.PDV = txtBoxPDV.Text;
                prehrambeniProizvod.SifraProizvoda =(long)Convert.ToDouble(txtBoxSifraProizvoda.Text);
                prehrambeniProizvod.Alergeni = txtBoxAlergeni.Text;
                prehrambeniProizvod.RokTrajanja = txtBoxRokTrajanja.Text;
                prehrambeniProizvod.NetoKolicina = textBoxNetoKolicina.Text;
                proizvod = prehrambeniProizvod;

            }
            else if ("Neprehrambeni proizvod".Equals(cmboxVrstaProizvoda.Items[cmboxVrstaProizvoda.SelectedIndex].ToString()))
            {
                NeprehrambeniProizvod neprehrambeniProizvod = new NeprehrambeniProizvod();
                neprehrambeniProizvod.Naziv = txtBoxNaziv.Text;
                neprehrambeniProizvod.Proizvodac = txtBoxProizvodac.Text;
                neprehrambeniProizvod.PDV = txtBoxPDV.Text;
                neprehrambeniProizvod.SifraProizvoda = (long)Convert.ToDouble(txtBoxSifraProizvoda.Text, System.Globalization.CultureInfo.InvariantCulture);
                neprehrambeniProizvod.Cijena = Convert.ToDouble(txtBoxCijena.Text, System.Globalization.CultureInfo.InvariantCulture); //neki format ili nesto za pretvaranje https://stackoverflow.com/questions/11399439/converting-string-to-double-in-c-sharp  
                neprehrambeniProizvod.SmartUredaj = chBoxSmart.Checked;
                neprehrambeniProizvod.Jamstvo = chBoxJamstvo.Checked;
                proizvod = neprehrambeniProizvod;
            }
            else
            {
                MessageBox.Show("Nije odabrana vrsta proizvoda.");
            }

            List<Proizvod> proizvodi = Proizvod.citajSve(nazivSkladista);
            proizvodi.Add(proizvod);

            Proizvodi myForm = new Proizvodi(nazivSkladista, proizvodi);
            this.Hide();
            myForm.ShowDialog();
            this.Close();

        }

        //provjeriti koji je odabran
        //ako je odabran neprehrambeni onda sakriti alergeni, ...
        private void cmboxVrstaProizvoda_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmboxVrstaProizvoda.SelectedIndex > -1)
            {
                // This will enable the button so long as the selected value
                // is not null or an empty string.
                if (cmboxVrstaProizvoda.SelectedItem != null && !string.IsNullOrEmpty(cmboxVrstaProizvoda.SelectedItem.ToString()))
                {
                    btnSpremi.Enabled = true;
                }
                else
                {
                    btnSpremi.Enabled = false;
                }

                if ("Prehrambeni proizvod".Equals(cmboxVrstaProizvoda.Items[cmboxVrstaProizvoda.SelectedIndex].ToString()))
                {
                    chBoxSmart.Hide();
                    chBoxJamstvo.Hide();
                    txtBoxAlergeni.Show();
                    txtBoxRokTrajanja.Show();
                    textBoxNetoKolicina.Show();
                }
                else if ("Neprehrambeni proizvod".Equals(cmboxVrstaProizvoda.Items[cmboxVrstaProizvoda.SelectedIndex].ToString()))
                {
                    txtBoxAlergeni.Hide();
                    txtBoxRokTrajanja.Hide();
                    textBoxNetoKolicina.Hide();
                    chBoxSmart.Show();
                    chBoxJamstvo.Show();
                }

            }
        }


        //provjera za cijenu, cijena moze biti samo decimalni broj. Moze se dodati samo jedna tocka
        private void txtBoxCijena_KeyPress(object sender, KeyPressEventArgs e)
        {
           char ch = e.KeyChar;
            if(ch==46 && txtBoxCijena.Text.IndexOf('.') != -1){
                e.Handled = true;
                return;
            }
            if (!char.IsDigit(ch) && ch!=8 && ch!=46)
            {
                e.Handled = true;
            }
        }


        //samo brojevi se mogu upisati u taj txtbox, zato jer je sifra proizvoda broj (long)
        private void txtBoxSifraProizvoda_KeyPress(object sender, KeyPressEventArgs e)
        {

            if ((!char.IsNumber(e.KeyChar) && (!char.IsControl(e.KeyChar))))
            {
                e.Handled = true;
            }
        }
    }
}