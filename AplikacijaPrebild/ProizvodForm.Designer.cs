﻿namespace AplikacijaPrebild
{
    partial class ProizvodForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblProizvod = new System.Windows.Forms.Label();
            this.lblVrsta = new System.Windows.Forms.Label();
            this.lblNaziv = new System.Windows.Forms.Label();
            this.lblProizvodac = new System.Windows.Forms.Label();
            this.lblPDV = new System.Windows.Forms.Label();
            this.lblSifraProizvoda = new System.Windows.Forms.Label();
            this.lblCijena = new System.Windows.Forms.Label();
            this.lblJamstvo = new System.Windows.Forms.Label();
            this.lblSmart = new System.Windows.Forms.Label();
            this.lblAlergeni = new System.Windows.Forms.Label();
            this.lblRokTrajanja = new System.Windows.Forms.Label();
            this.lblNetoKolicina = new System.Windows.Forms.Label();
            this.btnSpremi = new System.Windows.Forms.Button();
            this.btnOdustani = new System.Windows.Forms.Button();
            this.cmboxVrstaProizvoda = new System.Windows.Forms.ComboBox();
            this.txtBoxNaziv = new System.Windows.Forms.TextBox();
            this.txtBoxProizvodac = new System.Windows.Forms.TextBox();
            this.txtBoxPDV = new System.Windows.Forms.TextBox();
            this.txtBoxAlergeni = new System.Windows.Forms.TextBox();
            this.txtBoxSifraProizvoda = new System.Windows.Forms.TextBox();
            this.txtBoxRokTrajanja = new System.Windows.Forms.TextBox();
            this.txtBoxCijena = new System.Windows.Forms.TextBox();
            this.textBoxNetoKolicina = new System.Windows.Forms.TextBox();
            this.chBoxJamstvo = new System.Windows.Forms.CheckBox();
            this.chBoxSmart = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // lblProizvod
            // 
            this.lblProizvod.AutoSize = true;
            this.lblProizvod.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProizvod.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblProizvod.Location = new System.Drawing.Point(322, 0);
            this.lblProizvod.Name = "lblProizvod";
            this.lblProizvod.Size = new System.Drawing.Size(91, 24);
            this.lblProizvod.TabIndex = 1;
            this.lblProizvod.Text = "Proizvod";
            // 
            // lblVrsta
            // 
            this.lblVrsta.AutoSize = true;
            this.lblVrsta.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVrsta.Location = new System.Drawing.Point(56, 44);
            this.lblVrsta.Name = "lblVrsta";
            this.lblVrsta.Size = new System.Drawing.Size(104, 16);
            this.lblVrsta.TabIndex = 2;
            this.lblVrsta.Text = "Vrsta proizvoda:";
            // 
            // lblNaziv
            // 
            this.lblNaziv.AutoSize = true;
            this.lblNaziv.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNaziv.Location = new System.Drawing.Point(56, 80);
            this.lblNaziv.Name = "lblNaziv";
            this.lblNaziv.Size = new System.Drawing.Size(44, 16);
            this.lblNaziv.TabIndex = 3;
            this.lblNaziv.Text = "Naziv:";
            // 
            // lblProizvodac
            // 
            this.lblProizvodac.AutoSize = true;
            this.lblProizvodac.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProizvodac.Location = new System.Drawing.Point(56, 114);
            this.lblProizvodac.Name = "lblProizvodac";
            this.lblProizvodac.Size = new System.Drawing.Size(78, 16);
            this.lblProizvodac.TabIndex = 4;
            this.lblProizvodac.Text = "Proizvodač:";
            // 
            // lblPDV
            // 
            this.lblPDV.AutoSize = true;
            this.lblPDV.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPDV.Location = new System.Drawing.Point(56, 155);
            this.lblPDV.Name = "lblPDV";
            this.lblPDV.Size = new System.Drawing.Size(38, 16);
            this.lblPDV.TabIndex = 5;
            this.lblPDV.Text = "PDV:";
            // 
            // lblSifraProizvoda
            // 
            this.lblSifraProizvoda.AutoSize = true;
            this.lblSifraProizvoda.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSifraProizvoda.Location = new System.Drawing.Point(56, 195);
            this.lblSifraProizvoda.Name = "lblSifraProizvoda";
            this.lblSifraProizvoda.Size = new System.Drawing.Size(100, 16);
            this.lblSifraProizvoda.TabIndex = 6;
            this.lblSifraProizvoda.Text = "Šifra proizvoda:";
            // 
            // lblCijena
            // 
            this.lblCijena.AutoSize = true;
            this.lblCijena.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCijena.Location = new System.Drawing.Point(56, 239);
            this.lblCijena.Name = "lblCijena";
            this.lblCijena.Size = new System.Drawing.Size(48, 16);
            this.lblCijena.TabIndex = 7;
            this.lblCijena.Text = "Cijena:";
            // 
            // lblJamstvo
            // 
            this.lblJamstvo.AutoSize = true;
            this.lblJamstvo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblJamstvo.Location = new System.Drawing.Point(56, 276);
            this.lblJamstvo.Name = "lblJamstvo";
            this.lblJamstvo.Size = new System.Drawing.Size(61, 16);
            this.lblJamstvo.TabIndex = 8;
            this.lblJamstvo.Text = "Jamstvo:";
            // 
            // lblSmart
            // 
            this.lblSmart.AutoSize = true;
            this.lblSmart.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSmart.Location = new System.Drawing.Point(56, 318);
            this.lblSmart.Name = "lblSmart";
            this.lblSmart.Size = new System.Drawing.Size(86, 16);
            this.lblSmart.TabIndex = 9;
            this.lblSmart.Text = "Smart uređaj:";
            // 
            // lblAlergeni
            // 
            this.lblAlergeni.AutoSize = true;
            this.lblAlergeni.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAlergeni.Location = new System.Drawing.Point(56, 362);
            this.lblAlergeni.Name = "lblAlergeni";
            this.lblAlergeni.Size = new System.Drawing.Size(60, 16);
            this.lblAlergeni.TabIndex = 10;
            this.lblAlergeni.Text = "Alergeni:";
            // 
            // lblRokTrajanja
            // 
            this.lblRokTrajanja.AutoSize = true;
            this.lblRokTrajanja.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRokTrajanja.Location = new System.Drawing.Point(56, 405);
            this.lblRokTrajanja.Name = "lblRokTrajanja";
            this.lblRokTrajanja.Size = new System.Drawing.Size(82, 16);
            this.lblRokTrajanja.TabIndex = 11;
            this.lblRokTrajanja.Text = "Rok trajanja:";
            // 
            // lblNetoKolicina
            // 
            this.lblNetoKolicina.AutoSize = true;
            this.lblNetoKolicina.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNetoKolicina.Location = new System.Drawing.Point(56, 446);
            this.lblNetoKolicina.Name = "lblNetoKolicina";
            this.lblNetoKolicina.Size = new System.Drawing.Size(88, 16);
            this.lblNetoKolicina.TabIndex = 12;
            this.lblNetoKolicina.Text = "Neto količina:";
            // 
            // btnSpremi
            // 
            this.btnSpremi.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnSpremi.Enabled = false;
            this.btnSpremi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSpremi.ForeColor = System.Drawing.Color.Green;
            this.btnSpremi.Location = new System.Drawing.Point(12, 519);
            this.btnSpremi.Name = "btnSpremi";
            this.btnSpremi.Size = new System.Drawing.Size(200, 30);
            this.btnSpremi.TabIndex = 13;
            this.btnSpremi.Text = "Spremi";
            this.btnSpremi.UseVisualStyleBackColor = false;
            this.btnSpremi.Click += new System.EventHandler(this.btnSpremi_Click);
            // 
            // btnOdustani
            // 
            this.btnOdustani.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnOdustani.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOdustani.ForeColor = System.Drawing.Color.Red;
            this.btnOdustani.Location = new System.Drawing.Point(572, 519);
            this.btnOdustani.Name = "btnOdustani";
            this.btnOdustani.Size = new System.Drawing.Size(200, 30);
            this.btnOdustani.TabIndex = 14;
            this.btnOdustani.Text = "Odustani";
            this.btnOdustani.UseVisualStyleBackColor = false;
            this.btnOdustani.Click += new System.EventHandler(this.btnOdustani_Click);
            // 
            // cmboxVrstaProizvoda
            // 
            this.cmboxVrstaProizvoda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmboxVrstaProizvoda.FormattingEnabled = true;
            this.cmboxVrstaProizvoda.Items.AddRange(new object[] {
            "Prehrambeni proizvod",
            "Neprehrambeni proizvod"});
            this.cmboxVrstaProizvoda.Location = new System.Drawing.Point(242, 39);
            this.cmboxVrstaProizvoda.Name = "cmboxVrstaProizvoda";
            this.cmboxVrstaProizvoda.Size = new System.Drawing.Size(289, 21);
            this.cmboxVrstaProizvoda.TabIndex = 15;
            this.cmboxVrstaProizvoda.SelectedIndexChanged += new System.EventHandler(this.cmboxVrstaProizvoda_SelectedIndexChanged);
            // 
            // txtBoxNaziv
            // 
            this.txtBoxNaziv.Location = new System.Drawing.Point(242, 76);
            this.txtBoxNaziv.Name = "txtBoxNaziv";
            this.txtBoxNaziv.Size = new System.Drawing.Size(289, 20);
            this.txtBoxNaziv.TabIndex = 18;
            // 
            // txtBoxProizvodac
            // 
            this.txtBoxProizvodac.Location = new System.Drawing.Point(242, 110);
            this.txtBoxProizvodac.Name = "txtBoxProizvodac";
            this.txtBoxProizvodac.Size = new System.Drawing.Size(289, 20);
            this.txtBoxProizvodac.TabIndex = 19;
            // 
            // txtBoxPDV
            // 
            this.txtBoxPDV.Location = new System.Drawing.Point(242, 151);
            this.txtBoxPDV.Name = "txtBoxPDV";
            this.txtBoxPDV.Size = new System.Drawing.Size(289, 20);
            this.txtBoxPDV.TabIndex = 20;
            // 
            // txtBoxAlergeni
            // 
            this.txtBoxAlergeni.Location = new System.Drawing.Point(242, 358);
            this.txtBoxAlergeni.Name = "txtBoxAlergeni";
            this.txtBoxAlergeni.Size = new System.Drawing.Size(289, 20);
            this.txtBoxAlergeni.TabIndex = 21;
            // 
            // txtBoxSifraProizvoda
            // 
            this.txtBoxSifraProizvoda.Location = new System.Drawing.Point(242, 191);
            this.txtBoxSifraProizvoda.Name = "txtBoxSifraProizvoda";
            this.txtBoxSifraProizvoda.Size = new System.Drawing.Size(289, 20);
            this.txtBoxSifraProizvoda.TabIndex = 22;
            this.txtBoxSifraProizvoda.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBoxSifraProizvoda_KeyPress);
            // 
            // txtBoxRokTrajanja
            // 
            this.txtBoxRokTrajanja.Location = new System.Drawing.Point(242, 401);
            this.txtBoxRokTrajanja.Name = "txtBoxRokTrajanja";
            this.txtBoxRokTrajanja.Size = new System.Drawing.Size(289, 20);
            this.txtBoxRokTrajanja.TabIndex = 23;
            // 
            // txtBoxCijena
            // 
            this.txtBoxCijena.Location = new System.Drawing.Point(242, 235);
            this.txtBoxCijena.Name = "txtBoxCijena";
            this.txtBoxCijena.Size = new System.Drawing.Size(289, 20);
            this.txtBoxCijena.TabIndex = 24;
            this.txtBoxCijena.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBoxCijena_KeyPress);
            // 
            // textBoxNetoKolicina
            // 
            this.textBoxNetoKolicina.Location = new System.Drawing.Point(242, 446);
            this.textBoxNetoKolicina.Name = "textBoxNetoKolicina";
            this.textBoxNetoKolicina.Size = new System.Drawing.Size(289, 20);
            this.textBoxNetoKolicina.TabIndex = 25;
            // 
            // chBoxJamstvo
            // 
            this.chBoxJamstvo.AutoSize = true;
            this.chBoxJamstvo.Location = new System.Drawing.Point(242, 276);
            this.chBoxJamstvo.Name = "chBoxJamstvo";
            this.chBoxJamstvo.Size = new System.Drawing.Size(41, 17);
            this.chBoxJamstvo.TabIndex = 26;
            this.chBoxJamstvo.Text = "DA";
            this.chBoxJamstvo.UseVisualStyleBackColor = true;
            // 
            // chBoxSmart
            // 
            this.chBoxSmart.AutoSize = true;
            this.chBoxSmart.Location = new System.Drawing.Point(242, 316);
            this.chBoxSmart.Name = "chBoxSmart";
            this.chBoxSmart.Size = new System.Drawing.Size(41, 17);
            this.chBoxSmart.TabIndex = 27;
            this.chBoxSmart.Text = "DA";
            this.chBoxSmart.UseVisualStyleBackColor = true;
            // 
            // ProizvodForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.chBoxSmart);
            this.Controls.Add(this.chBoxJamstvo);
            this.Controls.Add(this.textBoxNetoKolicina);
            this.Controls.Add(this.txtBoxCijena);
            this.Controls.Add(this.txtBoxRokTrajanja);
            this.Controls.Add(this.txtBoxSifraProizvoda);
            this.Controls.Add(this.txtBoxAlergeni);
            this.Controls.Add(this.txtBoxPDV);
            this.Controls.Add(this.txtBoxProizvodac);
            this.Controls.Add(this.txtBoxNaziv);
            this.Controls.Add(this.cmboxVrstaProizvoda);
            this.Controls.Add(this.btnOdustani);
            this.Controls.Add(this.btnSpremi);
            this.Controls.Add(this.lblNetoKolicina);
            this.Controls.Add(this.lblRokTrajanja);
            this.Controls.Add(this.lblAlergeni);
            this.Controls.Add(this.lblSmart);
            this.Controls.Add(this.lblJamstvo);
            this.Controls.Add(this.lblCijena);
            this.Controls.Add(this.lblSifraProizvoda);
            this.Controls.Add(this.lblPDV);
            this.Controls.Add(this.lblProizvodac);
            this.Controls.Add(this.lblNaziv);
            this.Controls.Add(this.lblVrsta);
            this.Controls.Add(this.lblProizvod);
            this.Name = "ProizvodForm";
            this.Text = "ProizvodForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblProizvod;
        private System.Windows.Forms.Label lblVrsta;
        private System.Windows.Forms.Label lblNaziv;
        private System.Windows.Forms.Label lblProizvodac;
        private System.Windows.Forms.Label lblPDV;
        private System.Windows.Forms.Label lblSifraProizvoda;
        private System.Windows.Forms.Label lblCijena;
        private System.Windows.Forms.Label lblJamstvo;
        private System.Windows.Forms.Label lblSmart;
        private System.Windows.Forms.Label lblAlergeni;
        private System.Windows.Forms.Label lblRokTrajanja;
        private System.Windows.Forms.Label lblNetoKolicina;
        private System.Windows.Forms.Button btnSpremi;
        private System.Windows.Forms.Button btnOdustani;
        private System.Windows.Forms.ComboBox cmboxVrstaProizvoda;
        private System.Windows.Forms.TextBox txtBoxNaziv;
        private System.Windows.Forms.TextBox txtBoxProizvodac;
        private System.Windows.Forms.TextBox txtBoxPDV;
        private System.Windows.Forms.TextBox txtBoxAlergeni;
        private System.Windows.Forms.TextBox txtBoxSifraProizvoda;
        private System.Windows.Forms.TextBox txtBoxRokTrajanja;
        private System.Windows.Forms.TextBox txtBoxCijena;
        private System.Windows.Forms.TextBox textBoxNetoKolicina;
        private System.Windows.Forms.CheckBox chBoxJamstvo;
        private System.Windows.Forms.CheckBox chBoxSmart;
    }
}