﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AplikacijaPrebild
{
    internal class PrehrambeniProizvod : Proizvod, CijenaSPopustom, Ispisi
    {
        public String Alergeni { get; set; }
        public String RokTrajanja { get; set; }
        public String NetoKolicina { get; set; }


        public PrehrambeniProizvod() { }
        

        [JsonConstructor]
        public PrehrambeniProizvod(double cijena, string naziv, string proizvodac, string PDV, long sifraProizvoda,String alergeni, String rokTrajanja,String netoKolicina) : base(cijena, naziv, proizvodac, PDV, sifraProizvoda)
        {
            this.Alergeni =alergeni;
            this.RokTrajanja = rokTrajanja;
            this.NetoKolicina = netoKolicina;
        }
        public PrehrambeniProizvod(String naziv, int cijena, String proizvodac, String alergeni, String rokTrajanja, String PDV, long sifraProizvoda) : base(cijena, naziv, proizvodac, PDV, sifraProizvoda)
        {
            this.Alergeni = alergeni;
            this.RokTrajanja = rokTrajanja;
        }





        
        public void IzracunajCijenuSPopustom(double popust, double cijena)
        {
            double cijenaPopusta = (popust / 100) * cijena;
            double cijenaSPopustom = cijena - cijenaPopusta;
            Console.WriteLine("Cijena s popustom prehrambenog proizvoda je:" + cijenaSPopustom);
        }

        public String Ispisi()
        {
           return "Naziv: " + this.Naziv + ", Cijena: "+ this.Cijena + " kn, "+ "Alergeni: " + this.Alergeni + ", Rok trajanja: " + this.RokTrajanja + ", Neto kolicina: " + this.NetoKolicina;

        }
    }
    
}
