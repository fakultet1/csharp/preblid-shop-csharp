﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AplikacijaPrebild
{
    public abstract class Proizvod
    {
        public double Cijena { get; set; }
        public String Naziv { get; set; }
        public String Proizvodac { get; set; }
        public String PDV { get; set; }
        public long SifraProizvoda { get; set; }


        //konstruktor

        public Proizvod() : this(0, "", "", "", 0) { }
        public Proizvod(double cijena, string naziv, string proizvodac, string PDV, long sifraProizvoda)
        {
            this.Cijena = cijena;
            this.Naziv = naziv;
            this.Proizvodac = proizvodac;
            this.PDV = PDV;
            this.SifraProizvoda = sifraProizvoda;
        }

      

        public Proizvod(double cijena, String naziv, String proizvodac)
        {
            this.Proizvodac= proizvodac;
            this.Cijena= cijena;
            this.Naziv = naziv;
        }

        public Proizvod (String naziv, double cijena)
        {
            this.Naziv = naziv;
            this.Cijena = cijena;
        }


        //cita sve proizvode, prima naziv skladista da bi znao koji proizvode treba citati. Cita proizvode samo iz svog odabranog skladista
        public static List<Proizvod> citajSve(String nazivSkladista)
          
        {
            //citanje jsona
            List<Proizvod> proizvodi= new List<Proizvod>();
            string json = File.ReadAllText(@"D:\Fakultet\gitRepositories\preblid-shop-csharp\AplikacijaPrebild\JsonDatoteke\" + nazivSkladista +".json");
            List<JObject> proizvodiJson = JsonConvert.DeserializeObject<List<JObject>>(json);

            foreach(var proizvodJson in proizvodiJson)
            {
                if (proizvodJson.GetValue("vrsta").ToString().Equals("neprehrambeni")){
                    NeprehrambeniProizvod np = proizvodJson.ToObject<NeprehrambeniProizvod>();
                    //NeprehrambeniProizvod np = JsonConvert.DeserializeObject<NeprehrambeniProizvod>(proizvodJson.ToString());
                    proizvodi.Add(np);
                }
                else if (proizvodJson.GetValue("vrsta").ToString().Equals("prehrambeni"))
                {
                    PrehrambeniProizvod pp = proizvodJson.ToObject<PrehrambeniProizvod>();
                    proizvodi.Add(pp);
                }
                else
                {
                    MessageBox.Show("Nije navedena vrsta proizvoda ");
                    throw new Iznimka("Nije navedena vrsta proizvoda, " + proizvodJson.GetValue("naziv").ToString());  //koristenje exceptiona
                    
                }
               
                
            }
            return proizvodi;
        }
       
        //cita samo neprehrambene proizvode, filtriranje
        public static List<Proizvod> citajNeprehrambene(String nazivSkladista)
        {
            //citanje jsona
            List<Proizvod> proizvodi = new List<Proizvod>();
            string json = File.ReadAllText(@"D:\Fakultet\gitRepositories\preblid-shop-csharp\AplikacijaPrebild\JsonDatoteke\" + nazivSkladista + ".json");
            List<JObject> proizvodiJson = JsonConvert.DeserializeObject<List<JObject>>(json);

            foreach (var proizvodJson in proizvodiJson)
            {
                if (proizvodJson.GetValue("vrsta").ToString().Equals("neprehrambeni"))
                {
                    NeprehrambeniProizvod np = proizvodJson.ToObject<NeprehrambeniProizvod>();
                    proizvodi.Add(np);
                }
            }
            return proizvodi;
        }


        //cita samo prehrambene proizvode,filtriranje
        public static List<Proizvod> citajPrehrambene(String nazivSkladista)
        {
            //citanje jsona
            List<Proizvod> proizvodi = new List<Proizvod>();
            string json = File.ReadAllText(@"D:\Fakultet\gitRepositories\preblid-shop-csharp\AplikacijaPrebild\JsonDatoteke\" + nazivSkladista + ".json");
            List<JObject> proizvodiJson = JsonConvert.DeserializeObject<List<JObject>>(json);

            foreach (var proizvodJson in proizvodiJson)
            {
                if (proizvodJson.GetValue("vrsta").ToString().Equals("prehrambeni"))
                {
                    NeprehrambeniProizvod np = proizvodJson.ToObject<NeprehrambeniProizvod>();
                    proizvodi.Add(np);
                }
               
            }
            return proizvodi;
        }

        private static void spremiSveProizvode(List<Proizvod> proizvodi, String nazivSkladista)
        {
            try {
                List<JObject> proizvodiJson = new List<JObject>();
                foreach (Proizvod proizvod in proizvodi)
                {
                    if (proizvod is NeprehrambeniProizvod neprehrambeniProizvod)
                    {
                        JObject neprehrambeniProizvodJson = JObject.FromObject(proizvod);
                        neprehrambeniProizvodJson.Add(new JProperty("vrsta", "neprehrambeni"));
                        proizvodiJson.Add(neprehrambeniProizvodJson);
                    }
                    else if (proizvod is PrehrambeniProizvod prehrambeniProizvod)
                    {
                        JObject prehrambeniProizvodJson = JObject.FromObject(proizvod);
                        prehrambeniProizvodJson.Add(new JProperty("vrsta", "prehrambeni"));
                        proizvodiJson.Add(prehrambeniProizvodJson);
                    }
                }
                //Thread.Sleep(30000);
                //pretvara listu objekata u JSON

                String json = JsonConvert.SerializeObject(proizvodiJson, Formatting.Indented);
                File.WriteAllText(@"D:\Fakultet\gitRepositories\preblid-shop-csharp\AplikacijaPrebild\JsonDatoteke\" + nazivSkladista + ".json", json);
            }
            catch (Exception e)
            {
                throw new Iznimka("Dogodila se greska pri spremanju -" +e);
            }
        }

        public static void spremiProizvode(List<Proizvod> proizvodi, String nazivSkladista)
        {
            ThreadSpremiProizvode threadSpremiProizvode = new ThreadSpremiProizvode(spremiSveProizvode);
            threadSpremiProizvode.Invoke(proizvodi, nazivSkladista);
        }
        delegate void ThreadSpremiProizvode(List<Proizvod> proizvodi, String nazivSkladista);

    }
}
